# os-containers

These are very basic OS containers that are updated daily.

To pull these containers, run `podman pull` or `docker pull` followed by:

* **Fedora 30**: registry.gitlab.com/majorhayden/os-containers/fedora30:latest
* **Fedora 29**: registry.gitlab.com/majorhayden/os-containers/fedora29:latest
